from django.shortcuts import render
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from todos.models import TodoList, TodoItem
from django.urls import reverse_lazy

# Create your views here.


class TodoListView(ListView):
    model = TodoList
    template_name = 'todos/list.html'


class TodoListDetailView(DetailView):
    model = TodoList
    template_name = 'todos/detail.html'
    context_object_name = "object"


class TodoListCreateView(CreateView):
    model = TodoList
    template_name = 'todos/create.html'
    fields = ["name"]

    def get_success_url(self):
        return reverse_lazy("todos_detail", args=[self.object.id])


# class TodoListDeleteView(DeleteView):
#     model = TodoList
#     template_name = "todos/delete.html"
#     success_url = reverse_lazy("todos_list")


class TodoListUpdateView(UpdateView):
    model = TodoList
    template_name = "todos/update.html"
    fields = ["name"]

    # def get_success_url(self):
    #     return reverse_lazy("todos_detail", args=[self.object.id])


