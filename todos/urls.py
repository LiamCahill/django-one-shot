from django.urls import path
# from todos.views import  TodoListDetailView, TodoListDeleteView,
from todos.views import TodoListView, TodoListDetailView, TodoListCreateView, TodoListUpdateView
urlpatterns = [
    path("", TodoListView.as_view(), name="todos_list"),
    path("<int:pk>/", TodoListDetailView.as_view(), name="todos_detail"),
    path("create/", TodoListCreateView.as_view(), name="todos_create"),
    path("update/", TodoListUpdateView.as_view(), name="todos_update"),
]
